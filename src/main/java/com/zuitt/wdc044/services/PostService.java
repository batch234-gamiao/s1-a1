package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // create a post
    void createPost(String stringToken, Post post);
    // retrieve posts
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Activity s5
    ResponseEntity deletePost(Long id, String stringToken);

    Iterable<Post> getPosts();

    // Activity s5
    Iterable<Post> getMyPosts(String stringToken);


}
