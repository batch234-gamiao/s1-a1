package com.zuitt.wdc044.models;

import java.io.Serializable;

public class JwtRequest implements Serializable {
    // properties
    private static final long serialVersionUID = 5926468583005150707L;

    private String username;
    private String password;

    // constructors
    public JwtRequest(){}

    // parameterized constructors
    public JwtRequest(String username, String password){
        this.setUsername(username);
        this.setPassword(password);
    }

    // getters
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
    // setters
    public String setUsername(String username){
        return this.username = username;
    }
    public String setPassword(String password){
        return this.password = password;
    }
}
