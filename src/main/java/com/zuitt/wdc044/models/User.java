// Session 2 : Activity
package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name= "users")

public class User {
    // id
    @Id
    @GeneratedValue
    private Long id;

    /* I used a_variable, b_variable naming convention because Hibernate generates columns in alphabetical order.
    table:
    id | password | username

    The table should look like the ff if I used the naming convention said above:
    id | username | password
    */
    // username
    @Column
    private String username;

    // password
    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    //to avoid infinite recursion
    @JsonIgnore
    private Set<Post> posts;

    public Set <Post> getPosts(){
        return posts;
    }

    // constructors
    public User(){}
    // parameterized constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // getters
    public Long getId(){
        return id;
    }
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
    // setters
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
}
